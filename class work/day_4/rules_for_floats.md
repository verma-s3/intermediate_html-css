# Rules for floats

1. Use clear: left, clear: right, or clear: both to allow a block element to clear other block elements

2. Use an inner div, also floated, to contain other floated divs... the inner div's background color with grow with the floated divs within it.

